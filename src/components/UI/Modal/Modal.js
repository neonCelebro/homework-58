import React from 'react';
import './Modal.css';
import Wrapper from "../../../hoc/Wrapper";
import Backdrop from "../Backdrop/Backdrop";
import Button from '../Button/Button';

const btnConfiguration = (props) =>{
  if (props.btnConfiguration !== undefined) {
     return (
       props.btnConfiguration.map((btn, id) =>{
      return <Button key={id} btnType={btn.type} clicked={btn.clicked}>{btn.label}</Button>
    })
  )
  }
};

const Modal = props => (
  <Wrapper>
    <Backdrop show={props.show}
      clicked={props.hide}/>
    <div className='Modal' style={{
      transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
      opacity : props.show ? '1' : '0'
    }}>
      <div className='title'>{props.title}</div>
      {props.children}
      {btnConfiguration(props)}
    </div>
  </Wrapper>
);

export default Modal;
