import React from 'react';
import './Alert.css';
import Wrapper from '../../../hoc/Wrapper';

const dismissed = (props) =>{
  if (props.dismissed !== undefined) {
      return <button onClick={props.dismissed}>X</button>
    }
};

const Alert = props => (
  <Wrapper>
    <div
      style={{
        display : props.show? 'block' : 'none'
      }}
      onClick={props.hide} className='alertWraper'></div>
    <div
      style={{
        transform: props.show? 'translateY(0)' : 'translateY(-100vh)',
        opacity : props.show? '1' : '0'
      }}
      className={['Alert', props.type].join(' ')}>{props.children}{dismissed(props)}</div>
  </Wrapper>
);

export default Alert;
