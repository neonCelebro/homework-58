import React from 'react';
import './BuildControls.css';
import BuildControl from "./BuildControl/BuildControl";


const BuildControls = props => {
  return (
    <div className="BuildControls">

      <BuildControl/>
      <BuildControl/>
      <BuildControl/>
      <BuildControl/>
      <button 
              onClick={props.ordered}
              className='OrderButton'>ORDER NOW</button>
    </div>
  )
};

export default BuildControls;
