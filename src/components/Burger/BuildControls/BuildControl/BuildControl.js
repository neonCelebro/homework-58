import React from 'react';
import './BuildControl.css';

const BuildControl = props => {
 return (
   <div className="BuildControl">
     <button className="Less" disabled={true} >Less</button>
     <button className="More" >More</button>
   </div>
 );
};

export default BuildControl;
