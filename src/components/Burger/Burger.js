import React from 'react';
import './Burger.css';
import Ingredient from "./Ingredient/Ingredient";

const Burger = props => {

  return (
    <div className="Burger">
      <Ingredient/>
    </div>
  );
};

export default Burger;
