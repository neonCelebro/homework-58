import React, {Component} from 'react';
import Wrapper from "../../hoc/Wrapper";
import Burger from "../../components/Burger/Burger";
import BuildControls from "../../components/Burger/BuildControls/BuildControls";
import Modal from "../../components/UI/Modal/Modal";
import Alert from "../../components/UI/Alert/Alert";


class BurgerBuilder extends Component {
  state = {
    totalPrice: 20,
    purchasable: false,
    purchasing: false,
    alertShow: false,
  };

  updatePurchasableState = ingredients => {
    this.setState({purchasable:true});
  };

  purchaseHandler = () => {
    this.setState({purchasing: true})
  };

  purchaseCancelHandler = () => {
    this.setState({purchasing: false, alertShow: false});
  };

  purchaseContinueHandler = () => {
    this.setState({alertShow: true})
  };
  alertHide = () => {
    this.setState({alertShow : false})
  };



  render() {
    const disabledInfo = {...this.state.ingredients};

    for (let key in disabledInfo) {
      disabledInfo[key] = disabledInfo[key] <= 0;
    }

    return (
      <Wrapper>
        <Modal
          show={this.state.purchasing}
          hide={this.purchaseCancelHandler}
          title="Some kinda modal title"
          btnConfiguration={[
            {type: 'Success', label: 'Continue', clicked: this.purchaseContinueHandler},
            {type: 'Danger', label: 'Close', clicked: this.purchaseCancelHandler},
            {label: 'Any button with loose type'}
          ]}
        >
          <p>This is modal content</p>

        </Modal>
        <Alert hide={this.purchaseCancelHandler} dismissed={this.purchaseCancelHandler} type='success' show={this.state.alertShow} >Your order is accepted!</Alert>
        <Burger />
        <BuildControls

          purchasable={this.state.purchasable}
          ordered={this.purchaseHandler}
        />
      </Wrapper>
    )
  }
}

export default BurgerBuilder;
